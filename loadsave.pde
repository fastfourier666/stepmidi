// this reads the .ini file with all the settings and sets up local variables etc

boolean loadSettings(String prefsFile) {
  try {
    File inifile = new File(prefsFile);
    Wini ini = new Wini(inifile);
    String temp="";
    MIDI.clearAll();     // close any MIDI ports we have open
    dbgClear();          // clear the message window
    notes.clear();       // clear the list of notes
    thisNote=0;          // reset the note counter
    dbg (String.format ("Loading settings at: %02d:%02d:%02d\n",hour(), minute(), second()));
    
    // first load the MIDI file
    temp=ini.get ("stepmidi", "midifile");
    dbg (String.format("Loading %s...",temp));
    if (!loadMIDI(ini.get ("stepmidi", "midifile"))) {
      dbg ("Failed!\n");
      return false;
    }
    dbg("OK!\n");
    
    // open the MIDI output port
    temp=ini.get ("stepmidi", "midiout");   
    dbg (String.format("Opening MIDI output %s...",temp));
    if (!openMIDIOutput(temp)) {
      //dbg ("Failed!\n");
      return false;
    }
    dbg("OK!\n");
    
    // recall threshold and holdoff settings
    cThreshold.setValue (float(ini.get("stepmidi", "threshold")));
    cHoldoff.setValue (float(ini.get("stepmidi", "holdoff")));
    
    // recall loop and length settings
    loop = (ini.get("stepmidi", "loop").equals( "yes"));
    preserveLength = (ini.get("stepmidi", "preservelength").equals("yes"));

    dbg (String.format("Loop: %s\n", (loop ? "yes" : "no")));
    dbg (String.format("Preserve length? %s\n", (preserveLength? "yes" : "no")));
} 

  catch (IOException e) {
    dbg (String.format ("Error loading file %s", prefsFile));
    return false;
  }
  catch (NullPointerException e) {
    dbg ("whoooops. Probably some stuff is missing in the .ini file");
    return false;
  }
  return true;
}

// we just save the threshold and cutoff controls in the .ini file. Everything else is set manually
// by the user in a text editor.

void saveSettings(String prefsFile) {
  try {
    File inifile = new File(prefsFile, "");
    if (!inifile.exists()) {
      inifile.createNewFile();
    }  
    Wini ini = new Wini(inifile);
    
    ini.put ("stepmidi", "threshold", cThreshold.getValue());
    ini.put ("stepmidi", "holdoff", cHoldoff.getValue());

    ini.store();
  } 
  catch (Exception e) {
    println ("oops writing the ini file");
    println (e);
  }
}

// load and process the MIDI file!

public boolean loadMIDI(String filename) {
try {
    Sequencer sequencer = MidiSystem.getSequencer(); // Get the default Sequencer
    if (sequencer==null) {
      dbg("Sequencer device not supported");
      return false;
    } 
    sequencer.open();
    File f = new File (dataPath(filename));      
    Sequence sequence = MidiSystem.getSequence(f);
    sequencer.setSequence(sequence);
    if (sequence.getTracks().length > 1) {
      dbg ("only type 0 (single-track) MIDI files are supported");
      return false;
    }

    // figure out timing stuff. All the notes in a MIDI file are timed with
    // ticks/pulses. We need to figure out how long a tick/pulse is in real time
    float ppqn = (float)sequence.getResolution();
    float mpq = sequencer.getTempoInMPQ();
    float mpt = mpq / ppqn;                      // microseconds per tick ("pulse")

    print (String.format("ppqn: %f \n", ppqn));
    print (String.format("usqn: %f \n", mpq));
    print (String.format("us/p: %f \n", mpt));

    
    Track t = sequence.getTracks()[0];          // get the first (and only) track in the file    
    
    // Create one Note class instance for every Note On message we find, and add it to the list
    
    for (int i=0; i<t.size(); i++) {
      MidiEvent ev = t.get(i);
      MidiMessage message = ev.getMessage();
      if (message instanceof ShortMessage) {
        ShortMessage sm = (ShortMessage) message;
        if (sm.getCommand() == NOTE_ON && sm.getData2() > 0) {
          Note n = new Note(MIDI);
          n.tick = ev.getTick();        // get the start position so we can compute length later        
          n.pitch = sm.getData1();      // note pitch
          n.velocity = sm.getData2();   // note velocity
          n.eventIndex = i;             // store the index so we can correlate with a note off later
          notes.add(n);
        }
      }
    }

    // For each of the notes we just found, look for the corresponding note off
    // and compute the length of the note
    for (int i=0; i<notes.size(); i++) {
      Note n = notes.get(i);
      for (int e=n.eventIndex; e<t.size(); e++) {
        MidiEvent ev = t.get(e);
        MidiMessage message = ev.getMessage();
        if (message instanceof ShortMessage) {
          ShortMessage sm = (ShortMessage) message;
          if ((sm.getCommand() == NOTE_OFF) || (sm.getCommand() == NOTE_ON && sm.getData2() == 0)) {
            if (sm.getData1() ==n.pitch) {
              n.len = ev.getTick() - n.tick;
              n.lenMicroseconds = (long)(n.len * mpt);
              break;
            }
          }
        }
        if (e==(t.size()-1)) println ("NO MATCHING NOTE OFF");
      }
    }
  } 
  //catch (MidiUnavailableException | InvalidMidiDataException | IOException ex) {
  catch (FileNotFoundException ex) {
    dbg (String.format ("%s not found!\n", filename));
    return false;
  }
    catch (Exception ex) {
    println ("bad shit");
    ex.printStackTrace();
  }
  return true; 
}
