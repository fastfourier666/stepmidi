# stepmidi

A step MIDI file player for u/counterbrockage

### Compatibility

It's written on a Mac but should work on other OS. The one exception is that the "edit config" button runs Mac OS TextEdit, so on Windows/Linux you'll have to load the .ini file manually into your text editor of choice.

### Installation

Extract or clone to a folder named "stepmidi" in the Processing sketch folder. On a Mac, the full path to the main "stepmidi.pde" sketch file will be something like:
`/Users/YOURNAME/Documents/Processing/stepmidi/stepmidi.pde`

### Dependencies

Install from the Processing Contribution Manager (Sketch -> Import Library -> Add Library):
* The MidiBus
* ControlP5
* Sound


Install manually (copy to ~/Documents/Processing/libraries folder)
* [ini4j](https://www.dropbox.com/s/c7u2qsnh4ec3n2r/ini4j.zip?dl=0)


### Operation

This Processing sketch will play through a MIDI file one note/chord at a time, using an incoming audio level as a trigger.\
Audio is read from the first channel (left) of the system default audio input device.

Most settings are set in an .ini file in the sketch's data directory. Use the "edit config" button to change stuff in TextEdit, quit and save the file, then back in stepmidi click "reload config".

If the audio signal gets louder than the threshold level, the next note(s) in the file will be triggered. Set the "hold" control to prevent retriggering.\
The setting of these two controls gets saved into the same .ini file every time they are adjusted.

### Preparing MIDI Files

Files must be type 0 (single track). 

If chords are used in the source MIDI file, quantize them. Only notes with the exact same start time ("tick" or "pulse") will be played together.

Only note on and note off messages are supported (for now).
