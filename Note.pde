// This class handles the data and playing of a single note in the MIDI file.
// One of these gets created for each note (duh)

class Note {
  public long tick;                  // note start tick
  public int pitch;                  // MIDI pitch
  public int velocity;               // MIDI velocity
  public long len;                   // length in ticks
  public int eventIndex;             // the note's position in the original MIDI file
  public long lenMicroseconds;       // computed length of note in us
  private MidiBus midi;              // a reference to our "real" MIDI output so we can generate MIDI messages

  // The class constructor. When creating this Note we tell it about our MIDI output
  public Note(MidiBus m) {
    midi = m;
  }

  // just in case we want to dump this note's info to a log or something
  public String toString() {
    return String.format("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n", eventIndex, tick, pitch, velocity, len, lenMicroseconds);
  }

  // we want to play this note. 
  public void trig(boolean setTimer) {
    // send the note on message
    midi.sendNoteOn (0, pitch, velocity);

    // if we are preserving note length, set a timer to turn the note off again
    if (setTimer) {
      Timer timer = new Timer();
      timer.schedule (new StopNote(midi, pitch), Math.floorDiv(lenMicroseconds, 1000));
    }
  }
}

// We make one of these classes when the above timer starts. We give it a reference to our MIDI output, and a note pitch.
// When the timer expires, run() gets executed. Its only task is to turn the note off and then exit.

public class StopNote extends TimerTask {
  private MidiBus mb;
  private int pitch;
  StopNote (MidiBus mid, int pitch) {
    this.mb = mid;
    this.pitch = pitch;
  }
  public void run() {
    mb.sendNoteOff (0, pitch, 0);
    this.cancel();
  }
}
