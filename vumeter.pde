// draw a VU meter with a slow decay. Don't know why I wrote this, there are better ways of doing it

class VUMeter {
  private int _x, _y, _width, _height;
  private float value;

  VUMeter (int x, int y, int width_, int height_) {
    _x=x;
    _y=y;
    _width=width_;
    _height=height_;
  }

  public void render() {
    rectMode (CORNER);
    noStroke();
    fill (0, 255, 0);
    rect (_x, _y, Math.round (value * _width), _height);

    noFill();
    stroke (255);
    rect (_x, _y, _width, _height);
    if (value>0) value-=0.005;
  }

  public void update (float newValue) {
    if (newValue > value) value=newValue;
  }
}
