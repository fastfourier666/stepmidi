// try to open MIDI input.
boolean openMIDIInput(String portName) {
  final String[] availableInputs = MIDI.availableInputs();
  boolean found=false;
  for (int i=0; i<availableInputs.length; i++) {
    if (availableInputs[i].equals(portName)) {
      found=true;
      break;
    }
  }
  if (found) {
    // OK, we found a port with the name, now try opening it
    if (MIDI.addInput(portName))
      return true;
    else
      dbg ("found port but it failed to open\n");
  } else {
    dbg ("no port with that name!\nAvailable inputs are:\n");
    dbg (availableInputs);
  }
  return false;
}

// try to open MIDI output.
boolean openMIDIOutput(String portName) {
  final String[] availableOutputs = MIDI.availableOutputs();
  boolean found=false;
  for (int i=0; i<availableOutputs.length; i++) {
    if (availableOutputs[i].equals(portName)) {
      found=true;
      break;
    }
  }
  if (found) {
    // OK, we found a port with the name, now try opening it
    if (MIDI.addOutput(portName))
      return true;
    else
      dbg ("found port but it failed to open\n");
  } else {
    dbg ("no port with that name!\nAvailable outputs are:\n");
    dbg (availableOutputs);
    return false;
  }
  return false;
}