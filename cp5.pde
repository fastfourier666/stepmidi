ControlP5 cp5;

Slider cThreshold, cHoldoff;
Button cManualTrigger, cResetSequence, cReloadConfig, cEditConfig;
Textarea consoleTextArea;

void initcp5() {
  cp5=new ControlP5(this);

  PFont pfont = createFont("Arial", 10, true); // use true/false for smooth/no-smooth
  ControlFont font = new ControlFont(pfont, 14);

  cReloadConfig = cp5.addButton("reloadconfig")
    .setPosition (10, 455)
    .setSize (100, 30)
    .setCaptionLabel ("reload config");
  ;
  cEditConfig = cp5.addButton("editconfig")
    .setPosition (120, 455)
    .setSize (100, 30)
    .setCaptionLabel ("edit config");
  ;

  cResetSequence = cp5.addButton("resetsequence")
    .setPosition (10, 495)
    .setSize (100, 30)
    .setCaptionLabel ("reset sequence");
  ; 

  cResetSequence = cp5.addButton("manualtrigger")
    .setPosition (120, 495)
    .setSize (100, 30)
    .setCaptionLabel ("manual trigger");
  ; 

  cThreshold = cp5.addSlider("threshold")
    .setPosition (100, 40)
    .setSize (280, 20)
    .setRange (0,1)
    .setCaptionLabel ("")
    .setTriggerEvent(Slider.RELEASE)
    ;
    
  cHoldoff = cp5.addSlider("holdoff")
    .setPosition (100, 70)
    .setSize (280, 20)
    .setRange (0,500)
    .setCaptionLabel ("")
    .setTriggerEvent(Slider.RELEASE)
    ;
  consoleTextArea = cp5.addTextarea("txt")
    .setPosition(10, 145)
    .setSize(380, 300)
    .setFont(createFont("Monaco", 10))
    .setLineHeight(14)
    .setColor(color(0, 255, 1))
    .setColorBackground(color(0, 0, 0.05))
    ;
  
}

void controlEvent(ControlEvent theEvent) {
  if (theEvent.getName() == "reloadconfig") {
    settingsOK = loadSettings (dataPath("prefs.ini"));
  } else if (theEvent.getName() == "editconfig") {
    println (dataPath("prefs.ini"));
    exec ("/usr/bin/open", "-e", dataPath("prefs.ini"));
  } else if (theEvent.getName() == "threshold") {
    saveSettings (dataPath("prefs.ini"));
  }else if (theEvent.getName() == "holdoff") {
    saveSettings (dataPath("prefs.ini"));
  } else if (theEvent.getName() == "resetsequence") {
    MIDI.sendControllerChange(0, 123, 0); // all notes off!
    thisNote=0;
  } else if (theEvent.getName() == "manualtrigger") {
    playNext(true);
    trigIndicator=255;
  }
}

public void dbg(String text) {
  consoleTextArea.append(text);
  consoleTextArea.scroll(consoleTextArea.getText().length());
}

public void dbg(String[] text) {
  for (int i=0; i<text.length; i++) {
    consoleTextArea.append(text[i]+"\n");
  }
  consoleTextArea.scroll(consoleTextArea.getText().length());
}

public void dbgClear() {
  consoleTextArea.clear();
}
