import controlP5.*;
import themidibus.*;
import org.ini4j.*;
import java.util.*;
import processing.sound.*;
import javax.sound.midi.*;
import java.io.*;

public static final int NOTE_ON = 0x90;
public static final int NOTE_OFF = 0x80;

AudioIn input;
MidiBus MIDI;
Amplitude level;
VUMeter meter = new VUMeter (100, 10, 280, 20);
boolean settingsOK=false;
float inLevel;
long holdoffTimer;
boolean armed = true;
int trigIndicator;
List<Note>notes = new ArrayList<Note>();
int thisNote = 0;
boolean loop=false;
boolean preserveLength=false;
boolean playEnabled=true;

void setup() {
  size(400, 800);
  initcp5();
  MIDI = new MidiBus();
  settingsOK = loadSettings (dataPath("prefs.ini"));
  
  // Start the audio input
  input = new AudioIn (this, 0);      // left channel of the system default input
  input.start();
  input.amp(1.0);
  
  // Create an amplitude analysis object and attach it to the input we just opened
  level = new Amplitude (this);
  level.input (input);
}


void draw() {
  background(44);
  
  // draw some text labels
  textSize(18);
  fill(255);
  textAlign(RIGHT, CENTER);
  text ("in level", 90, 18);
  text ("threshold", 90, 48);
  text ("hold (ms)", 90, 78);
  textAlign(LEFT, CENTER);
  textSize(40);
  text (String.format ("next:#%d/%d", thisNote, notes.size()-1), 10, 110);

  // draw the meter
  inLevel = level.analyze();
  meter.update (level.analyze());
  meter.render();

  // handle trigger arm and threshold
  if (!armed && (millis() > holdoffTimer)) armed=true;
  if (armed) {
    if (inLevel > cThreshold.getValue()) {
      playNext(true);
      trigIndicator=255;
      holdoffTimer = millis() + (int)cHoldoff.getValue();
      armed=false;
    }
  }

  // draw the trigger indicator (red box)
  rectMode (CORNER);
  fill(255, 0, 0, trigIndicator);
  stroke(255);
  if (trigIndicator>0) trigIndicator-=25;
  if (trigIndicator<=0) trigIndicator=0;
  rect (345, 100, 35, 35);
}

// Handle what happens when we hit the plank or press the button.
//
//

void playNext(boolean killNotes) {
  if (thisNote>=0) {                           
    if (killNotes && !preserveLength)          // not playing notes with original length, so..
      MIDI.sendControllerChange(0, 123, 0);    // all notes off!
    Note n = notes.get(thisNote++);            // get current note data and bump the counter+1
    n.trig(preserveLength);                    // play the note and set the noteoff timer if needed
    if (thisNote==notes.size())                // have we reached the end?
      thisNote = (loop? 0 : -1) ;              // go back to note 0 (loop) or stick the counter at -1 (no loop) 
    else
      if (notes.get(thisNote).tick == n.tick) playNext(false);    // call this again with other notes at the same timestamp
  }
}
